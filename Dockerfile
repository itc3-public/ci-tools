FROM docker:latest

ENV KUBE_LATEST_VERSION="v1.10.2"
ENV HELM_VERSION="v2.9.0"
ENV GOPATH="/gopath"
ENV GOBIN="/gopath/bin"

RUN apk add --no-cache git go gcc libgcc libc-dev linux-headers libc-dev ca-certificates glide bash \
    && mkdir -p /gopath/bin \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && wget -q http://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && helm init --client-only \
    && helm repo add stable https://kubernetes-charts.storage.googleapis.com/ \
    && helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/ \
    && helm repo update


